@extends('base')
@section('title', 'Welcome to my laravel project')
@section('stylesheed')
<style type="text/css">
    
</style>
@endsection
@section('content')
	<table class="table">
		<thead>
			<tr>
				<td>Title</td>
				<td>Body</td>
				<td>Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($post as $p)
			<tr>
				<td><a href="{{ route('post.show', $p->id) }}">{{ $p->title }}</a></td>
				<td>{{ $p->body }}</td>
				<td><a href="">Update</a><a href="">Delete</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
@endsection
@section('script')
@endsection