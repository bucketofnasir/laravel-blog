@extends('base')
@section('title', 'View a single post')
@section('stylesheed')
<style type="text/css">
    h1{
    	font-weight: 400!important;
    }
</style>
@endsection
@section('content')
 <h1>{{ $post->title }}</h1>
 <p>{{ $post->body }}</p>
@endsection
@section('script')
@endsection