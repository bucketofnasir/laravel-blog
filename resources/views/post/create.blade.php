@extends('base')
@section('title', 'Create a new post')
@section('stylesheed')
<style type="text/css">
    h1{
    	font-weight: 400!important;
    }
</style>
@endsection
@section('content')
 <div class="row">
 	<div class="col-md-2"></div>
 	<div class="col-md-8">
 		<h1>Create new post</h1>
 		<hr>
 		{!! Form::open(array('route'=>'post.store')) !!}
 			{{ Form::label('title', "Title:") }}
 			{{ Form::text('title', null, array('class'=>'form-control')) }}
 			{{ Form::label('body', "Body:") }}
 			{{ Form::textarea('body', null, array('class'=>'form-control')) }}
 			{{ Form::submit('Create post', array('class'=>'btn btn-success btn-lg btn-block', 'style'=>'margin-top:20px')) }}
 		{!! Form::close() !!}
 	</div>
 </div>
@endsection
@section('script')
@endsection